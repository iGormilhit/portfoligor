# [portfoliGor][1]

**Archived repository. The new project address is
<https://git.milhit.ch/igor/portfoligor>.**

Yet another HUGO portfolio theme, that I'm writing to my own purpose on
<https://igor.milhit.ch>, published with the regular [MIT license][6].

It displays on the front page a personal and professional description, and ways
to contact the person. It has a blog section too.

The HTML `<head>` has a metadata section to expose metadata that are useful for
reference manager as Zotero (in Dublin Core) or to social media (see
[below][5]).

As the release version can tell, this theme is still in development and misses
lot of things [see the opened [issues][2]].

You can install it as the [HUGO documentation][4] recommends.

![Screenshot][7]

## Configuration

### Personal and professional description

The front page displays, if it exists, the content of the main index markdown
file that could be placed at the root of the content directory [[HUGO's
documentation][3]].

There's also three sections that can be displayed:

- The online presence.
- The personal contacts.
- The professional contacts.

It depends on the data provided by the site configuration, in the `params`
section. Below a comprehensive example:

```toml
[params]

    # Professional contact data
    [params.contactsPro]
    address = ""
    phone = "[as-used-by-a-phone]"
    mobile = "[as-used-by-a-phone]"
    email = ""
    cv = "[file-added-in-static-directory]"
    publickey = "[file-added-in-static-directory]"
    fingerprint = ""

    # Personal contact data
    [params.contactsPriv]
    address = ""
    phone = "[as-used-by-a-phone]"
    mobile = "[as-used-by-a-phone]"
    email = ""
    publickey = "[file-added-in-static-directory]"
    fingerprint = ""
    xmpp = "[handle]"

    # Web accounts
    [params.online]
    github = "[username]"
    framagit = "[username]"
    mastodon = "[URL]"
    twitter = "[username]"
    keybase = "[username]"
    wikipedia = "[french-site-username]"
    openstreetmap = "[username]"
    discogs = "[username]"
```

### Layout configuration

- Add `layout: verse` to the front header of a specific content to improve
  format for text in verse. Verse should be separated by a line break, strophes
  are paragraphs.
- Add `layout: static` to the front header of a static page, thus preventing to
  display the post metadata such as the publication date.

### Date management

You should set `enableGitInfo` to `True`, as it is used by the post metadata
display for the last modification date, and in the footer to provide a link to
the last commit modifying the content (still hard coded though, #14).

The publication date uses the `date` field of the front matter, that should be
automatically set by the `hugo new` command. If the content is published later,
you can add the `publishdate` field manually, and it will override the `date`
data.

All further modification dates should be provided by the `git` metadata. But
you can also set manually the `lastmod` date in the front matter.

To make it work, in the site configuration file, you should configure the date
management as below:

```toml
[frontmatter]
date = ["date", "publishDate"]
lastmod = [":git", "lastmod"]
```

### Social media metadata

An image for the website can be defined in the site configuration. Place an
image optimized for the web, in the 1200 × 600 pixels resolution, in the
`static/images/` directory. Then provide in the site configuration it's file
name and image description (max 80 chars, with a trailing dot):

```toml
[params]

    Description = "[Small website description]"
    Image = "mySite.jpg"
    ImageDescription = "[Alternate text]"
```

For each content (blog post), a social media image can be defined in the front
matter. The image have to be placed in the same directory (`static/images/`),
same resolution. The frontmatter is as follow:

```toml
description: "[post-description]"
postimage: "[file-name.extension]"
postimagedescription: "[Alternate text]"
```

[1]: https://git.milhit.ch/igor/portfoligor
[2]: https://git.milhit.ch/igor/portfoligor/issues
[3]: https://gohugo.io/templates/homepage/#add-content-and-front-matter-to-the-homepage
[4]: https://gohugo.io/getting-started/quick-start/#step-3-add-a-theme
[5]: #social-media-metadata
[6]: https://git.milhit.ch/igor/portfoligor/src/branch/dev/LICENSE
[7]: screenshots/theme-example.png "Screenshot of my own site using this theme"
